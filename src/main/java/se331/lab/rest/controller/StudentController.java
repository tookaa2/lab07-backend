package se331.lab.rest.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Controller
public class StudentController {
    List<Student> students;
    public StudentController(){
        this.students = new ArrayList<>();
        this.students.add(Student.builder()
                .id(1L)
                .studentId("SE-001")
                .name("Prayuth")
                .surname("The Minister")
                .gpa(3.59)
                .image("http://13.250.41.39/images/tu.jpg")
                .penAmount(15)
                .description("The greatest man ever!")
                .build());
        this.students.add(Student.builder()
                .id(2L)
                .studentId("SE-002")
                .name("Cherprang")
                .surname("BNK48")
                .gpa(4.01)
                .image("http://13.250.41.39/images/cherprang.png")
                .penAmount(2)
                .description("Code for Thailand")
                .build());
        this.students.add(Student.builder()
                .id(3L)
                .studentId("SE-003")
                .name("Nobi")
                .surname("Nobita")
                .gpa(1.77)
                .image("http://13.250.41.39/images/nobita.gif")
                .penAmount(0)
                .description("Welcome to Olympic!")
                .build());
    }

    @GetMapping("/students")
    public ResponseEntity getAllStudent(){
        return ResponseEntity.ok(students);
    }
    @GetMapping("/students/{id}")
    public ResponseEntity getStudentById(@PathVariable("id")Long id){
        return ResponseEntity.ok(students.get(Math.toIntExact(id-1)));
    }

    @PostMapping("/students")
    public ResponseEntity saveStudent(@RequestBody Student student){
        student.setId((long) this.students.size());
        this.students.add(student);
        return ResponseEntity.ok(student);
    }


}
